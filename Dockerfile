# use debian base
FROM debian:buster-slim

# update
RUN set -e; \
	apt-get update; \
	apt-get -y upgrade; \
	:

RUN apt-get update ; apt-get -y install wget make tar p7zip-full squashfs-tools vim \
	e2fsprogs parted dosfstools acpica-tools mtools \
	device-tree-compiler xz-utils sudo gcc libssl-dev python2 python3 \
	bison flex u-boot-tools git bc fuseext2 e2tools multistrap \
	qemu-system-arm g++ cpio python unzip rsync

RUN git config --global user.name "LX2160A Toolchain Container"
RUN git config --global user.email "support@solid-run.com"